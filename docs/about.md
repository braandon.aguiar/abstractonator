![Image](https://crossref.org/img/labs/creature1.svg)

# Abstractonator

A game that tests if you can distinguish machine-generated content (MGC) from human-generated content (HGC). Specifically, it focuses on whether you can distingusih between machine-generated and human-written scholarly abstracts.

Click the `Play` button below to begin.

Your score will be kept on the left sidebar.

To restart the game, just reload the page.




