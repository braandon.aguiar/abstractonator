## FAQ

**Q:** The user interface of this game is terrible. Can you fix it?

**A:** Probably not. We are terrible at UI. And besides, this is a Crossref Labs project- It is meant to be irritating.

---

**Q:** Does this game support Oculus Quest 2?

**A:** No. We don't like Meta. They stole half of "metadata."

---

**Q:** Will this game be available on Steam Deck?

**A:** We're not sure it has enough power.

---

**Q:** Does this game support force-feedback?

**A:** If you drink enough.

---

**Q:** I am terrible at this game. Does this mean I am going to be replaced by AI?

**A:** Probably.
