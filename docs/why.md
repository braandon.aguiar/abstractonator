## Why?

You will have probably seen a lot of buzz around the recent progress that has been made in using ML-based models to generate content.

And maybe you've wondered what all of the fuss is about?

This game illustrates how one tool, OpenAI's GPT-3, can already be used to generate plausible scholarly content. And it is worth noting that GPT-3 is not even state-of-the-art. Other private models are supposed to be even more sophisticated- including the upcoming GPT-4.

What are the implications of these developments for scholarly communication? Well, I thought I'd let GPT-3 itself answer that question. I gave it the prompt:

`Write a paragraph describing the implications of using AI to generate scholarly content:`

And it produced the following text (I have not edited it):


> The implications of using AI to generate scholarly content are far-reaching and potentially game-changing. On the one hand, AI-generated content could help to level the playing field in terms of access to information and knowledge. For example, AI could be used to generate content for open access journals or to provide access to research that is behind paywalls. On the other hand, AI-generated content could also create new challenges and ethical concerns. For example, if AI is used to generate peer-reviewed articles, who would be responsible for the accuracy of the information? And if AI is used to generate content for journals, who would be responsible for ensuring that the content meets the standards of quality and rigor that are expected of scholarly publications?

 It will become almost trivial for malicious people to launch the equivalent of an "intellectual denial of service attack" on any industry that justifies itself by selecting and curating content.
 
 I think that we are going to see a flood of machine-generated submissions to publishers. Inevitably, some of them will evade detection and be published. At the very least, publishers will have a much harder time doing initial screenings of documents.

This isn't just a problem facing scholarly communication and publishing. This will launch an epistemic crisis across a large swathe of society and almost every industry.

And there will be an arms race of sorts between those AI/ML researchers creating tools to generate content and those creating tools to *detect* generated content.

So is Crossref going to be doing anything about this?

Yes. But obliquely. 

I don't think that we can be competitive in what will be a fiercely competitive race to develop tools to analyze content and determine whether is machine-generated.

The best we can do here is keep track of the state-of-the-art and work with others who are developing these tools.
 
But we will look to see if there are any approaches where we can use our metadata to detect attempts at large-scale abuse using machine-generated content. 