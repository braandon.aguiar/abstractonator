import json
import logging
import random
import os

import streamlit as st
from bs4 import BeautifulSoup

APP_NAME = "Abstractonator"

logging.basicConfig(level="INFO", format="%(asctime)s %(message)s")
logger = logging.getLogger(__name__)


def load_file(fn):
    with open(fn) as f:
        return f.read()

def load_records():
    with open("abstracts.json") as f:
        return json.load(f)


def strip_blank_lines(text):
    return os.linesep.join([s for s in text.splitlines() if s])


def strip_markup(text):
    return BeautifulSoup(text, features="html.parser").get_text()


def generate_abstracts():
    pass


def start_game():
    st.session_state.initalized = True


def show_sidebar():
    with st.sidebar:
        st.image("https://assets.crossref.org/logo/crossref-logo-landscape-200.png")
        st.header(APP_NAME)
        st.markdown("---")
        st.subheader("Score")
        st.metric(label="Tries", value=st.session_state.tries)
        st.metric(label="Correct", value=st.session_state.correct)
        st.metric(label="Wrong", value=st.session_state.wrong)
        with st.expander(label="Sample information"):
            st.text(f"sample size: {len(st.session_state.records)} records")


def show_splash():
    st.markdown(load_file('docs/about.md'))
    st.button(label="Play", on_click=start_game)
    st.markdown(load_file('docs/warning.md'))
    st.markdown(load_file('docs/why.md'))
    st.markdown(load_file('docs/how.md'))
    st.markdown(load_file('docs/faq.md'))


def generate_choices():
    abstracts = [
        {"source": "oai", "abstract": choice["text"]}
        for choice in st.session_state.current_record["openai_abstracts"]["choices"]
    ]
    abstracts.append(
        {
            "source": "crossref",
            "abstract": strip_markup(st.session_state.current_record["abstract"]),
        }
    )
    return abstracts


def new_question():
    st.session_state.current_record = random.choice(st.session_state.records)
    choices = generate_choices()
    random.shuffle(choices)
    st.session_state.abstracts = choices
    st.session_state.new_question = True
    logger.info(type(st.session_state.abstracts))


def score_choice():
    choice = st.session_state.current_choice
    st.session_state.tries += 1
    if choice is None:
        return
    st.session_state.answering_new_question = False
    st.session_state.next_question = False
    if st.session_state.abstracts[int(choice)]["source"] == "crossref":
        st.balloons()
        st.session_state.correct += 1
        st.session_state.last_answer_correct = True
    else:
        st.snow()
        st.session_state.wrong += 1
        st.last_answer_correct = False


def do_nothing():
    st.session_state.next_question = False


def try_again():
    reset_question()


def reset_score():
    st.session_state.tries = 0
    st.session_state.correct = 0
    st.session_state.wrong = 0


def reset_question():
    st.session_state.initialized = True
    st.session_state.answering_new_question = True
    st.session_state.next_question = True
    st.session_state.last_answer_correct = False


def show_instructions():
    
    st.info(
        """
    The title below is from a journal article in the Crossref database. Three of the four abstracts below were genrated by OpenAI's GPT-3. The other is the abstract that was submitted with the article metadata.

    Select the number of the abstract that you think is the **published** abstract.
    """
    )

def show_title():
    st.markdown("---")
    st.markdown(
        f"## _{st.session_state.current_record['title'][0]}_", unsafe_allow_html=True
    )

def show_controls():
    st.radio(
        label="Which is the published abstract?",
        options=[f"{count}" for count in range(4)],
        horizontal=True,
        key="current_choice",
        disabled=st.session_state.last_answer_correct,
        on_change=do_nothing,
    )
    st.button(
        label="Check answer",
        on_click=score_choice,
        disabled=st.session_state.last_answer_correct,
    )


def show_answer_status():
    pass




def show_status():
    if st.session_state.answering_new_question:
        return
    if st.session_state.last_answer_correct:
        st.success("You got it!")
        doi_uri = f"https://doi.org/{st.session_state.current_record['DOI']}"
        st.markdown(f"[{doi_uri}]({doi_uri})")
        st.button(label="Try another", on_click=try_again)
    elif st.session_state.new_question > 0:
        st.error("Nope.")


def show_abstracts():
    st.markdown("""---""")
    for count, choice in enumerate(st.session_state.abstracts):
        st.subheader(f"Abstract: {count}")
        st.markdown(strip_blank_lines(choice["abstract"]), unsafe_allow_html=True)


def show_choice():
    logger.info("Show choice")
    show_instructions()
    show_controls()
    show_status()
    show_title()
    show_abstracts()


if "initialized" in st.session_state:
    if st.session_state.next_question:
        new_question()
    show_choice()
else:
    st.session_state.records = load_records()
    show_splash()
    reset_score()
    reset_question()


show_sidebar()
